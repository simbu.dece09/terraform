provider "aws" {
  region     = "${var.aws_region}"
}

resource "aws_lb" "back_end" {
  name               = "${var.load_balancer_name}"
  internal           = "${var.internal}"
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.appservers.id}"]
  subnets            = ["${var.subnets}"]

  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group" "back_end" {
  name = "Target-Group-for-Backend"
  port = "${var.backend_port}"
  protocol = "${var.backend_protocol}"
  vpc_id = "${var.vpc_id}"
}

resource "aws_lb_listener" "appserver" {
  load_balancer_arn = "${aws_lb.back_end.arn}"
  port              = "${var.appserver_port}"
  protocol          = "${var.appserver_protocol}"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.back_end.arn}"
  }
}

output "Application load balancer Endpoint" {
  value = "${aws_lb.back_end.dns_name}"
}

resource "aws_launch_configuration" "for_appserver" {
  name = "appsever-autoscaling-gp"
  image_id = "${data.aws_ami.ubuntu.id}"
  instance_type = "${lookup(var.instance_type,var.env)}"
  user_data     = "${file("install_httpd.sh")}"
  security_groups = ["${aws_security_group.appservers.id}"]
  key_name      = "${var.key_name}"
  root_block_device
  {
    volume_type = "${var.volume_type}"
    volume_size = "${var.volume_size}"  }
}

resource "aws_autoscaling_group" "appserver" {
  name                      = "${var.asg_name}"
  max_size                  = "${var.max_size}"
  min_size                  = "${var.min_size}"
  health_check_grace_period = "${var.grace_period}"
  health_check_type         = "${var.health_check_type}"
  desired_capacity          = "${var.desired_capacity}"
  force_delete              = "${var.force_delete}"
  launch_configuration      = "${aws_launch_configuration.for_appserver.name}"
  vpc_zone_identifier       = ["${var.subnets}"]
  target_group_arns         = ["${aws_lb_target_group.back_end.arn}"]
}

data "aws_ami" "ubuntu" {
  most_recent = "true"
  filter
  {
    name = "name"
    values = ["amzn2-ami-hvm-2.0.20181008-x86_64-gp2"]
  }
  filter
  {
    name = "owner-alias"
    values = ["amazon"]
  }
}

resource "aws_security_group" "appservers" {
  name        = "Http"
  description = "Allow Http all inbound traffic"
  vpc_id      = "${var.vpc_id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "Tcp"
    cidr_blocks = ["${var.cidr_block}"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "Tcp"
    cidr_blocks = ["${var.cidr_block}"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "Tcp"
    cidr_blocks = ["${var.cidr_block}"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["${var.cidr_block}"]
  }
}
