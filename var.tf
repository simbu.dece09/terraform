variable "aws_region" {
  default = "us-west-2"
}

variable "instance_type" {
  type = "map"
  default = {
    dev = "t2.micro"
    test = "t2.medium"
  }
}
variable "env" { default = "dev" }
variable "internal" {
  default = true
}
variable "subnets" {
  type = "list"
  default = ["subnet-08c6eb297dc3ca824", "subnet-0b16f4a45e606a66d"]
}
variable "backend_port" {
  default = 80
}
variable "backend_protocol" { default = "HTTP" }
variable "vpc_id" {
  default = "vpc-04d4ab7d48567c972"
}
variable "appserver_port" {
  default = 80
}
variable "appserver_protocol" {
  default = "HTTP"
}
variable "volume_type" { default = "gp2" }
variable "volume_size" { default = 30 }
variable "asg_name" { default = "Scania Test" }
variable "max_size" { default = 5 }
variable "min_size" { default = 2 }
variable "grace_period" {
  default = 30
}
variable "health_check_type" {
  default = "EC2"
}
variable "desired_capacity" {
  default = "2"
}
variable "force_delete" {
  default = "true"
}
variable "load_balancer_name" { default = "Scaniaproject" }
variable "key_name" {
  default = "tf_demo"
}
variable "cidr_block" {
  type = "list"
  default = ["0.0.0.0/0"]
}
